/* copy paste from BBD */
/* https://github.com/rauenzi/BetterDiscordApp/blob/master/js/main.js */
const path = require('path');
const fs = require('fs');
const Module = require('module').Module;
function stripBOM(content) {
  if (content.charCodeAt(0) === 0xfeff) {
    content = content.slice(1);
  }
  return content;
}
function isEmpty(obj) {
  if (obj == null || obj == undefined || obj == '') return true;
  if (typeof obj !== 'object') return false;
  if (Array.isArray(obj)) return obj.length == 0;
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
function testJSON(data) {
  try {
    return JSON.parse(data);
  } catch (err) {
    return false;
  }
}
function extractMeta(content) {
  const firstLine = content.split('\n')[0];
  const hasOldMeta = firstLine.includes('//META');
  if (hasOldMeta) return parseOldMeta(content);
  const hasNewMeta = firstLine.includes('/**');
  if (hasNewMeta) return parseNewMeta(content);
  throw 'META was not found.';
}

function parseOldMeta(content) {
  const meta = content.split('\n')[0];
  const rawMeta = meta.substring(meta.lastIndexOf('//META') + 6, meta.lastIndexOf('*//'));
  if (meta.indexOf('META') < 0) throw 'META was not found.';
  const parsed = testJSON(rawMeta);
  if (!parsed) throw 'META could not be parsed.';
  if (!parsed.name) throw 'META missing name data.';
  parsed.format = 'json';
  return parsed;
}

function parseNewMeta(code/* : string */)/* : BDPluginManifest */ {
  const ret = {};
  let key = '';
  let value = '';
  try {
    const jsdoc = code.substr(code.indexOf('/**') + 3, code.indexOf('*/') - code.indexOf('/**') - 3);
    for (let i = 0, lines = jsdoc.split(/[^\S\r\n]*?(?:\r\n|\n)[^\S\r\n]*?\*[^\S\r\n]?/); i < lines.length; i++) {
      const line = lines[i];
      if (!line.length) continue;
      if (line[0] !== '@' || line[1] === ' ') {
        value += ` ${line.replace('\\n', '\n').replace(/^\\@/, '@')}`;
        continue;
      }
      if (key && value) ret[key] = value.trim();
      const spaceSeperator = line.indexOf(' ');
      key = line.substr(1, spaceSeperator - 1);
      value = line.substr(spaceSeperator + 1);
    }
    ret[key] = value.trim();
    ret.format = 'jsdoc';
  } catch (err) {
    throw new /* ErrorNoStack */Error(`Plugin META header could not be parsed ${err}`);
  }
  if (!ret.name) throw new /* ErrorNoStack */Error('Plugin META header missing name property');
  return ret;
}

Module._extensions['.js'] = function(module, filename) {
  if (!fs.existsSync(filename)) return Reflect.apply(originalRequire, this, arguments);
  let content = fs.readFileSync(filename, 'utf8');
  content = stripBOM(content);

  const meta = extractMeta(content);
  meta.filename = path.basename(filename);
  module._compile(content, module.filename);
  if (isEmpty(module.exports)) content += `\n\nmodule.exports = ${meta.exports || meta.name};`;
  module._compile(content, filename);
};

global.BdApi = {
  findModuleByProps: () => {
    return { PureComponent: class {} };
  }
};

const pluginsDir = fs.readdirSync('public/plugins/');
let result = '';
for (let pluginFile of pluginsDir) {
  const ImportedPlugin = require(path.resolve(__dirname, 'public/plugins/', pluginFile));
  const Plugin = new ImportedPlugin();
  result += `<div id="${pluginFile.substr(0, pluginFile.length - 10)}">
            <div class="mdl-card mdl-cell mdl-cell--12-col">
                <div class="mdl-card__media mdl-color-text--grey-50" />
                <h4>${Plugin.getName()}</h4>
            </div>
            <div class="mdl-card__supporting-text">
                ${Plugin.getDescription()}
            </div>
            <div class="mdl-card__supporting-text sub-text">
                Version: ${Plugin.getVersion()}
            </div>
            <div class="mdl-color-text--white-600 mdl-card__buttons">
                <a href="plugins/${pluginFile}" download
                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    download
                </a>
                <a href="https://gitlab.com/_Lighty_/bdstuff/blob/master/public/plugins/${pluginFile}" target="_blank"
                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    view source
                </a>
            </div>
        </div>
        `;
}
let template = fs.readFileSync('public/index_template.html', 'utf8');
template = template.replace('{PLUGINS}', result);
fs.writeFileSync('public/index.html', template, 'utf8');
